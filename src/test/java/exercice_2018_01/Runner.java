package exercice_2018_01;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;
import java.util.Arrays;

public class Runner {

	@Test
	public void test_d() {

		AppTest instance = new AppTest();
		Class appClass = instance.getClass();

		Result result = JUnitCore.runClasses(appClass);

		// prepare the failed test list
		ArrayList<String> fails = new ArrayList<String>();
		for (Failure failure : result.getFailures()) {
			String str = failure.toString();
			str = str.substring(0, str.indexOf("("));
			fails.add(str);
		}

		Method[] methods = appClass.getMethods();
		String[] methodsName =  new String[methods.length];
		for (int i = 0; i < methods.length; i++) {
			methodsName[i] = methods[i].getName();	
		}

		Arrays.sort(methodsName);

		// get the test list
		for (int i = 0; i < methodsName.length; i++) {
			String testName = methodsName[i];
			boolean isTest = testName.contains("test");
			StringBuilder builder = new StringBuilder();
			if (isTest == true) {
				if (fails.indexOf(testName) != -1) {
					builder.append("@fail(Coderpower) : " + testName);
				} else {
					builder.append("@pass(Coderpower) : " + testName);
				}
			}
			String str = builder.toString();
			System.out.println(str);
		}

	}
}
