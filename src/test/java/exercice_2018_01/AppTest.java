package exercice_2018_01;

import static org.junit.Assert.assertArrayEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import static org.junit.Assert.fail;

@RunWith(Theories.class)
public class AppTest {

	public static @DataPoints("files") String[] paths = { "inputs/signal_S.csv", "inputs/signal_M.csv",
			"inputs/signal_L.csv", "inputs/signal_XL.csv" };
	// public static @DataPoints("files") String[] paths = { "inputs/signal_S.csv",
	// "inputs/signal_M.csv" };

	@Theory
	public void test1_construction_of_matrix_from_file(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		try {
			double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
			double[][] fromFileToMatrix2 = appToTest.fromFileToMatrix(path);
			assertArrayEquals(fromFileToMatrix, fromFileToMatrix2);
		} catch (Exception e) {
			System.out.println("[LOG] " +e);
			fail();
		} catch (AssertionError e) {
			System.out.println("[LOG] " +e);
			fail();
		}
	}

	@Theory
	public void test2_rounding_matrix(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		try {
			int[][] roundedMatrix = appRef.getRoundedMatrix(fromFileToMatrix);
			int[][] roundedMatrix2 = appToTest.getRoundedMatrix(fromFileToMatrix);
			assertArrayEquals(roundedMatrix, roundedMatrix2);
		} catch (Exception e) {
			System.out.println("[LOG] " + e);
			fail();
		} catch (AssertionError e) {
			System.out.println("[LOG] " +e);
			fail();
		}
	}

	@Theory
	public void test3_averaging_matrix(@FromDataPoints("files") String path) throws IOException {
		AppInterface appRef = new App();
		AppInterface appToTest = new AppCandidat();

		double[][] fromFileToMatrix = appRef.fromFileToMatrix(path);
		int[][] roundedMatrix = appRef.getRoundedMatrix(fromFileToMatrix);
		try {
			double[][] averagedCompressedMatrix = appRef.getAveragedCompressedMatrix(roundedMatrix);
			double[][] averagedCompressedMatrix2 = appToTest.getAveragedCompressedMatrix(roundedMatrix);
			assertArrayEquals(averagedCompressedMatrix, averagedCompressedMatrix2);
		} catch (Exception e) {
			System.out.println("[LOG] " +e);
			fail();
		} catch (AssertionError e) {
			System.out.println("[LOG] " +e);
			fail();
		}
	}

}
