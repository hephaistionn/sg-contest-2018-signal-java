package exercice_2018_01;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class AppCandidat implements AppInterface {

	public static void main(String[] args) throws IOException {
		AppCandidat appCandidat = new AppCandidat();
		// As we are not using static methods (to be able to test it), you can try to
		// run your code here if you want to
		/*
		 * appCandidat.computeResults(); double[][] fromFileToMatrix =
		 * appCandidat.fromFileToMatrix("4x4.csv"); Utils.printMatrix(fromFileToMatrix);
		 * int[][] roundedMatrix = appCandidat.getRoundedMatrix(fromFileToMatrix);
		 * Utils.printMatrix(roundedMatrix);
		 */
	}

	/**
	 * Cette méthode permet de tester les vôtres à partir de fichiers de test créé
	 * par la classe Init Malheureusement, vous ne pouvez pas tester ce code
	 * localement, il sert donc surtout à vous montrer l'enchaînement des
	 * traitements qui seront effectués par les tests successfis pour valider votre
	 * code
	 * 
	 * @throws IOException
	 */
	public void computeResults() throws IOException {
		int[] values = Init.values;
		for (int i : values) {
			double[][] inputMatrix = fromFileToMatrix(i + "x" + i + ".csv");
			int[][] roundedMatrix = getRoundedMatrix(inputMatrix);
			double[][] avgCompressedMatrix = getAveragedCompressedMatrix(roundedMatrix);
			Utils.fromMatrixToFile(roundedMatrix, i + "x" + i + "_round.csv");
			Utils.fromMatrixToFile(avgCompressedMatrix, i + "x" + i + "_compressed.csv");
		}
	}

	@Override
	public double[][] fromFileToMatrix(String path) throws IOException {
		// Le fichier utilise le point (.) comme séparateur de décimale et virgule (,)
		// comme séparateur de champs/valeurs sur une même ligne
		// Les sauts de ligne sont \n (format 'Linux')
		// NB : depuis Java 1.7, la nouvelle classe Files propose des méthodes statiques
		// intéressantes.
		// /!\ le code tourne en Java 1.7 donc les méthodes à partir de Java 1.8
		// n'existe pas encore ;)
		int hauteur = 0; // valeur à retrouver depuis le fichier
		int largeur = 0; // valeur à retrouver depuis le fichier
		double[][] inputMatrix = new double[hauteur][largeur];
		// A remplir correctement à partir des valeurs du fichier
		return inputMatrix;
	}

	@Override
	public int[][] getRoundedMatrix(double[][] inputMatrix) {
		int hauteur = 0; // valeur à retrouver depuis les paramètres
		int largeur = 0; // valeur à retrouver depuis les paramètres
		// Attention, il faut donc passer de l'état BigDecimal à un entier en
		// arrondissant correctement
		int[][] roundedMatrix = new int[hauteur][largeur];
		// A finir de remplir
		return roundedMatrix;
	}

	@Override
	public double[][] getAveragedCompressedMatrix(int[][] inputMatrix) {
		int hauteur = 0;
		int largeur = 0;
		// On teste déjà sur la matrice pourra être réduite, il faut pour cela que ses
		// dimensions soient paires (hauteur ET largeur)
		// Cette méthode permet de vérifier cela
		if (Utils.testBothDimensionsDividableByTwo(inputMatrix)) {
			double[][] outputMatrix = new double[hauteur][largeur];
			// A finir de remplir
			// NB : BigDecimal est un objet, il faut donc appeler des méthodes afin lui
			// faire subir des modifications / récupérer certaines propriétés...
			// Dans chaque case du tableau résultat, il faudra une moyenne de 4 cases
			// adjacentes (voir instructions pour détails)
			return outputMatrix;
		} else {
			return null;
		}
	}
}
