package exercice_2018_01;

import java.io.IOException;

public interface AppInterface {

	/**
	 * Cette méthode construit une matrice (array d'array, [][] en Java) de double à
	 * partir d'un fichier dont le path est spécifié
	 * 
	 * @param path
	 * @return
	 * @throws IOException
	 */
	double[][] fromFileToMatrix(String path) throws IOException;

	/**
	 * Cette méthode prend en entrée une matrice (tableau) de nombres décimaux et
	 * fourni la version avec des entiers lors d'un arrondi (de type half up, 0.5 ->
	 * 1)
	 * 
	 * @param inputMatrix
	 * @return
	 */
	int[][] getRoundedMatrix(double[][] inputMatrix);

	/**
	 * Cette méthode retourne un nouveau tableau, 2x plus petit (dans les 2
	 * dimensions) que le tableau fourni en entrée. Pour se faire, il stockera le
	 * résultat de 4 cases adjacentes dans la case cible La case [0][0] du résultat
	 * contiendra donc la moyenne des 4 cases [0][0], [0][1], [1][0], [1][1] du
	 * tableau d'entrée ATTENTION : chaque case du tableau d'entrée n'est utilisée
	 * qu'une seule fois pour le calcul du tableau de résultat
	 * 
	 * @param inputMatrix
	 * @return
	 */
	double[][] getAveragedCompressedMatrix(int[][] inputMatrix);

}
